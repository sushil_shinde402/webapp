﻿/// <reference path="angular.min.js" />



var app = angular.module("Demo", ["ngRoute"])
                 .config(function ($routeProvider) {
                     $routeProvider
                     .when("/home", {
                         templateUrl: "Templates/Home.html",
                         controller: "homeController"
                     })
                     .when("/courses", {
                         templateUrl: "Templates/courses.html",
                         controller: "coursesController"
                     })
                     .when("/students", {
                         templateUrl: "Templates/students.html",
                         controller: "studentsController"
                     })
                         
                 })
            
                    .controller("homeController", function ($scope) {
                        $scope.message = "Home Page";
                    })
                    .controller("coursesController", function ($scope) {
                        $scope.courses = ["C#", "Vb.Net", "ASP.NET", "SQL Server", "ASP.MVC"];
                    })
                    .controller("studentsController", function ($scope, $http) {
                        $http.get("StudentService.asmx/GetStudents")
                        .then(function (response) {
                            $scope.students = response.data;
                        })
                        
                    })